#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>

int dfs_search_file (const char *dir)
{
  DIR *dp;
  struct dirent *ep;
  struct stat statbuf;

  dp = opendir (dir);
  if (dp != NULL)
    {
	    while ((ep = readdir (dp))!=NULL)
	    {
	        if()
	        {//is dir
	      		if (strcmp(ep->d_name , ".")==0 || strcmp(ep->d_name , "..")==0)
	      		//if (ep->d_name[strlen(ep->d_name)-1]=='.')
	      		{
	      			continue;
	      		}
	      		//
	      		char * tmp = (char *)malloc(sizeof(char) * (strlen(ep->d_name) + strlen(dir)+2 ));
	      		strcpy(tmp,dir);
	      		strcat(tmp,"\\");
	      		strcat(tmp,ep->d_name);
	      		//
		        //puts (ep->d_name);
	      		dfs_search_file(tmp);
	      		free(tmp);
	      	}
	      	else if(ep->d_type==8)
	      	{
	      		char * tmp = (char *)malloc(sizeof(char) * (strlen(ep->d_name) + strlen(dir)+2 ));
	      		strcpy(tmp,dir);
	      		strcat(tmp,"\\");
	      		strcat(tmp,ep->d_name);
		        printf("%s\n", tmp);
		        free(tmp);
	      	}
      	}
      	(void) closedir (dp);
    }
    else
    {
    	printf ("Couldn't open the directory");
  	}

  return 0;
}


int main(int argc, char const *argv[])
{
	dfs_search_file("\\");
	return 0;
}
