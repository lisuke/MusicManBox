#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "Loader.h"
#include "Loader.cpp"

using namespace std;


class sql_test
{
public:
	static int call_back(void * data,int arg_count,char ** arg_values,char ** arg_names){
			std::cout<<arg_names[0]<<"\t"<<arg_names[1]<<std::endl;
		for (int i = 0; i < arg_count; ++i)
		{
			std::cout<<std::string("")+arg_values[i]<<std::endl;
		}
	}
};


bool sqlite3tools::execDQL(const std::string &dql)
{

	if (!open(db_path))
	{
		if(!open(db_path))return false;
	}
	// begin();
	// if(SQLITE_OK != sqlite3_exec(db,dql.c_str(),sql_test::call_back,(void *)(&dql_result),NULL))
	// 	return false;
	// commit();
	int ncols,nrows;
	char ** result;
	sqlite3_get_table(db,dql.c_str(),&result, &nrows, &ncols,  NULL);
	cout<<"行数: "<< nrows<<endl;
	cout<<"列数: "<< ncols<<endl;
	for (int i = 0; i < nrows; ++i)
	{
		for (int j = 0; j < ncols; ++j)
		{
			cout<< result[i*ncols+j] <<"\t";
		}
		cout<<endl;
	}
	return true;
}

void * sqlite3tools::get_query_result()
{

	return dql_result;
}
void sqlite3tools::get_m_suffix(const std::string table,std::vector<std::string> &suffix)
{

	if (!open(db_path))
	{
		if(!open(db_path))exit(-1) ;
	}
	int ncols,nrows;
	char ** result;
	std::string sql = "select * from ";
	sql+=table;

	sqlite3_get_table(db,sql.c_str(),&result, &nrows, &ncols,  NULL);

	for (int i = 0; i < nrows; ++i)
	{
		for (int j = 0; j < ncols; ++j)
		{
			suffix.push_back(result[i*ncols+j]);
		}
	}
}
/*
int main(int argc, char const *argv[])
{
	sqlite3tools lib("./test.db");
	// sqlite3tools<std::multimap,sql_test> lib("./test.db");
	lib.execDDL("create table test( id integer,name text)");
	lib.insert("insert into test values(22,'lisuke')");
	// lib.remove("delete from test  where name='liran'");
	// lib.update("update test set id=2  where name='liran'");
	lib.execDQL("select * from test");
	// lib.execDDL("drop table test ;");
	return 0;
}
*/
